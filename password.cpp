#include "password.h"
#include "ui_password.h"
#include <QDialog>

password::password(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::password)
{
    ui->setupUi(this);
    status_password = false;
    this->setWindowTitle("PASSWORD");
}

password::~password()
{
    delete ui;
}

void password::on_okBtn_clicked()
{

    status_password = true;
}

void password::on_cancelBtn_clicked()
{
    status_password = false;
}
