#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QObject>
#include <QMainWindow>
#include <QStackedWidget>
#include <QFile>
#include <QPushButton>



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


    
private slots:

//    QWidget spindleIniConfigPG_3 = new QWidget;

    void on_spindleBtn_clicked();

    void on_xAxisBtn_clicked();

    void on_yAxisBtn_clicked();

    void on_zAxisBtn_clicked();

    void on_trajAxisBtn_clicked();

    int file_presence(QString path);

    void on_reloadBtn_clicked();

    void on_importZBtn_clicked();

    void on_factoryResetBtn_clicked();

private:
    Ui::MainWindow *ui;



    QString md5_generation(QString md5path);

    int compare_md5(QString compare_md5path1,QString compare_md5path2);



};

#endif // MAINWINDOW_H
