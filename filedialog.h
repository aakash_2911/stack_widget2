#ifndef FILEDIALOG_H
#define FILEDIALOG_H

class QEvent;

#include <QFileDialog>
#include <QString>

class FileDialog : public QFileDialog
{
    Q_OBJECT
public:
    explicit FileDialog(QWidget *parent = 0,bool sl=false);

public:
    bool eventFilter(QObject *o, QEvent *e);
    void setTopDir(const QString &path);
    QString topDir() const;
    QString fileName;
    bool getOpenFileName (QString title,QString path, QString type);
    bool save_load;
signals:
    void saved_opened_file_name(QString name,bool sl);
private:
    bool pathFits(const QString &path) const;
private slots:
    void checkHistory();
    void checkGoToParent();
    void checkLineEdit(const QString &text);
private:
    QString mtopDir;
    QPushButton *myOpenButton;
};

#endif // FILEDIALOG_H
