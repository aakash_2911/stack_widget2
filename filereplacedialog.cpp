#include "filereplacedialog.h"
#include "ui_filereplacedialog.h"
#include <QFile>
#include <QDebug>


fileReplaceDialog::fileReplaceDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::fileReplaceDialog)
{
    ui->setupUi(this);
    status_file_transfer = false;
    this->setWindowTitle("Message");
//    status_file_transfer = fileReplaceDialog::on_okBtn_clicked();
    qDebug()<<"File transfer Status Ok Button"<<status_file_transfer;
//    status_file_transfer = fileReplaceDialog::on_cancelBtn_clicked();
//    qDebug()<<"File Transfer Status Cancel Button"<<status_file_transfer;

}

fileReplaceDialog::~fileReplaceDialog()
{
    delete ui;
}

void fileReplaceDialog::on_okBtn_clicked()
{
    this->close();
    status_file_transfer = true;
    qDebug()<<"dialog close";

}

void fileReplaceDialog::on_cancelBtn_clicked()
{

    this->close();
    status_file_transfer = false;
    qDebug()<<"Dialog cancel";

}



