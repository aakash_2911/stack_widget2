#-------------------------------------------------
#
# Project created by QtCreator 2016-07-15T16:19:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = stack_widget2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    filedialog.cpp \
    filereplacedialog.cpp \
    passworddialog.cpp

HEADERS  += mainwindow.h \
    filedialog.h \
    filereplacedialog.h \
    passworddialog.h

FORMS    += mainwindow.ui \
    filereplacedialog.ui \
    passworddialog.ui
