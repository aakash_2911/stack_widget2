#ifndef PASSWORD_H
#define PASSWORD_H

#include <QMainWindow>

namespace Ui {
class passwordDialog;
}

class passwordDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit passwordDialog(QWidget *parent = 0);
    ~passwordDialog();

    bool status_password;
    
private slots:
    void on_okBtn_clicked();

    void on_cancelBtn_clicked();

private:
    Ui::passwordDialog *ui;
};

#endif // PASSWORD_H
