#ifndef FILEREPLACEDIALOG_H
#define FILEREPLACEDIALOG_H

#include <QDialog>

namespace Ui {
class fileReplaceDialog;
}

class fileReplaceDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit fileReplaceDialog(QWidget *parent = 0);
    ~fileReplaceDialog();

    bool status_file_transfer;
private slots:

    void on_okBtn_clicked();

    void on_cancelBtn_clicked();

private:
    Ui::fileReplaceDialog *ui;

};

#endif // FILEREPLACEDIALOG_H
