#include <mainwindow.h>
#include <ui_mainwindow.h>
#include <QSettings>
#include <QtGui>
#include <QCryptographicHash>
#include <QStackedWidget>
#include <QDebug>
#include <QFile>
#include "errno.h"
#include <QFileInfo>
#include <QIODevice>
#include <QCoreApplication>
#include <QString>
#include <QTextStream>
#include <filedialog.h>
#include <filereplacedialog.h>
#include <passworddialog.h>



int currentIndex = 0;

//QSettings *qsettings = new QSettings("/home/automata/cnc_config/mydemo.inc", QSettings::IniFormat);


//QByteArray hashDataSrc;
//QByteArray hashDataTemp;
//QByteArray hashDataFinal;
//QFile fileSrc("/home/automata/cnc_config/mydemo.inc");
//QFile fileTemp("/home/automata/Temp_location/mydemotemp.inc");
////QFile fileFinal("/home/automata/Final_location/mydemofinal.inc");



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    currentIndex = 0;
    ui->statusBar->clearMessage();


    ui->sMaxRpmEdit_3->setReadOnly(true);
    ui->sMinRpmEdit_3->setReadOnly(true);
    ui->sEpsilonEdit_3->setReadOnly(true);
    ui->sInvertIndexEdit_3->setReadOnly(true);
    ui->sPPREdit_3->setReadOnly(true);
    ui->sScaleEdit_3->setReadOnly(true);
    ui->sTimeoutEdit_3->setReadOnly(true);

    ui->trajNoForceHomingEdit->setReadOnly(true);
    ui->trajMaxVelEdit_3->setReadOnly(true);
    ui->trajMaxAccEdit_3->setReadOnly(true);
    ui->trajLinearUnitsEdit->setReadOnly(true);
    ui->trajHomeEdit->setReadOnly(true);
    ui->trajDefVelEdit_3->setReadOnly(true);
    ui->trajDefAccEdit_3->setReadOnly(true);
    ui->trajCycleTimeEdit->setReadOnly(true);
    ui->trajCoordinatesEdit->setReadOnly(true);
    ui->trajAxisEdit->setReadOnly(true);
    ui->trajArcBlendRampFreqEdit->setReadOnly(true);
    ui->trajArcBlendOptiDepthEdit->setReadOnly(true);
    ui->trajArcBlendGapCyclesEdit->setReadOnly(true);
    ui->trajArcBlendFallbackEnableEdit->setReadOnly(true);
    ui->trajArcBlendEnableEdit->setReadOnly(true);
    ui->trajAngularUnitsEdit->setReadOnly(true);


    ui->xBacklashEdit_3->setReadOnly(true);
    ui->xCompFileTypeEdit->setReadOnly(true);
    ui->xFbScaleEdit->setReadOnly(true);
    ui->xFerrorEdit_3->setReadOnly(true);
    ui->xHomeEdit->setReadOnly(true);
    ui->xHomeLatchVelEdit->setReadOnly(true);
    ui->xHomeSearchVelEdit->setReadOnly(true);
    ui->xHomeSequenceEdit->setReadOnly(true);
    ui->xHomeUseDriveEdit->setReadOnly(true);
    ui->xHomeUseIndexEdit->setReadOnly(true);
    ui->xHomeVolatileEdit->setReadOnly(true);
    ui->xMaxAccEdit_3->setReadOnly(true);
    ui->xMaxLimitEdit_3->setReadOnly(true);
    ui->xMaxVelEdit_3->setReadOnly(true);
    ui->xMinFerrorEdit_3->setReadOnly(true);
    ui->xMinLimitEdit_3->setReadOnly(true);
    ui->xScaleEdit_3->setReadOnly(true);
    ui->xTypeEdit->setReadOnly(true);

    ui->yBacklashEdit_3->setReadOnly(true);
    ui->yCompFileTypeEdit->setReadOnly(true);
    ui->yFbScaleEdit->setReadOnly(true);
    ui->yFerrorEdit_3->setReadOnly(true);
    ui->yHomeEdit->setReadOnly(true);
    ui->yHomeLatchVelEdit->setReadOnly(true);
    ui->yHomeSearchVelEdit->setReadOnly(true);
    ui->yHomeSeqEdit->setReadOnly(true);
    ui->yHomeUseDriveEdit->setReadOnly(true);
    ui->yHomeUseIndexEdit->setReadOnly(true);
    ui->yHomeVolatileEdit->setReadOnly(true);
    ui->yMaxAccEdit_3->setReadOnly(true);
    ui->yMaxLimitEdit_3->setReadOnly(true);
    ui->yMaxVelEdit_3->setReadOnly(true);
    ui->yMinFerrorEdit_3->setReadOnly(true);
    ui->yMinLimitEdit_3->setReadOnly(true);
    ui->yScaleEdit_3->setReadOnly(true);
    ui->yTypeEdit->setReadOnly(true);
    ui->yHomeOffsetEdit->setReadOnly(true);


    ui->zBacklashEdit_3->setReadOnly(true);
    ui->zCompFileTypeEdit->setReadOnly(true);
    ui->zFbScaleEdit->setReadOnly(true);
    ui->zFerrorEdit_3->setReadOnly(true);
    ui->zHomeEdit->setReadOnly(true);
    ui->zHomeLatchVelEdit->setReadOnly(true);
    ui->zHomeSearchVelEdit->setReadOnly(true);
    ui->zHomeSeqEdit->setReadOnly(true);
    ui->zHomeUseDriveEdit->setReadOnly(true);
    ui->zHomeUseIndexEdit->setReadOnly(true);
    ui->zVolatileHomeEdit->setReadOnly(true);
    ui->zMaxAccEdit_3->setReadOnly(true);
    ui->zMaxLimitEdit_3->setReadOnly(true);
    ui->zMaxVelEdit_3->setReadOnly(true);
    ui->zMinFerrorEdit_3->setReadOnly(true);
    ui->zMinLimitEdit_3->setReadOnly(true);
    ui->zScaleEdit_3->setReadOnly(true);
    ui->zTypeEdit->setReadOnly(true);
    ui->zHomeOffsetEdit->setReadOnly(true);



}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_spindleBtn_clicked()
{
    ui->spindleBtn->setStyleSheet("color: red");
    ui->xAxisBtn->setStyleSheet("color: ");
    ui->yAxisBtn->setStyleSheet("color: ");
    ui->zAxisBtn->setStyleSheet("color: ");
    ui->trajAxisBtn->setStyleSheet("color: ");

    QSettings *qsettings = new QSettings("/home/automata/cnc_config/mydemo.inc", QSettings::IniFormat);
    qsettings->beginGroup("SPINDLE");
    ui->axisSelectionSTK->setCurrentIndex(0);
    currentIndex = 0;


    QString sMaxRpm = qsettings->value("MAX_RPM").toString();
    qDebug()<<sMaxRpm;

    QString sMinRpm = qsettings->value("MIN_RPM").toString();
    qDebug()<<sMinRpm;

    QString sPpr = qsettings->value("PPR").toString();
    qDebug()<<sPpr;

    QString sScale = qsettings->value("SCALE").toString();
    qDebug()<<sScale;

    QString sInvertIndex = qsettings->value("INVERT_INDEX").toString();
    qDebug()<<sInvertIndex;

    QString sEpsilon = qsettings->value("EPSILON").toString();
    qDebug()<<sEpsilon;

    QString sTimeout = qsettings->value("TIMEOUT").toString();
    qDebug()<<sTimeout;

    ui->sMaxRpmEdit_3->setText(sMaxRpm);
    ui->sMinRpmEdit_3->setText(sMinRpm);
    ui->sEpsilonEdit_3->setText(sEpsilon);
    ui->sInvertIndexEdit_3->setText(sInvertIndex);
    ui->sPPREdit_3->setText(sPpr);
    ui->sScaleEdit_3->setText(sScale);
    ui->sTimeoutEdit_3->setText(sTimeout);
    ui->statusBar->clearMessage();
    ui->statusBar->setStyleSheet("background-color: ");


}

void MainWindow::on_reloadBtn_clicked()
{
    if(currentIndex == 0)
    {
        on_spindleBtn_clicked();
    }
    else if(currentIndex == 1)
    {
        on_xAxisBtn_clicked();
    }
    else if(currentIndex == 2)
    {
        on_yAxisBtn_clicked();
    }
    else if(currentIndex == 3)
    {
        on_zAxisBtn_clicked();
    }
    else if(currentIndex == 4)
    {
        on_trajAxisBtn_clicked();
    }
    else
    {

    }
    ui->statusBar->clearMessage();
    ui->statusBar->setStyleSheet("background-color: ");
}

void MainWindow::on_trajAxisBtn_clicked()
{

    ui->spindleBtn->setStyleSheet("color: ");
    ui->xAxisBtn->setStyleSheet("color: ");
    ui->yAxisBtn->setStyleSheet("color: ");
    ui->zAxisBtn->setStyleSheet("color: ");
    ui->trajAxisBtn->setStyleSheet("color: red");
    QSettings *qsettings = new QSettings("/home/automata/cnc_config/mydemo.inc", QSettings::IniFormat);
    qsettings->beginGroup("TRAJ");
    ui->axisSelectionSTK->setCurrentIndex(4);
    currentIndex = 4;

    QString trajAngularUnits = qsettings->value("ANGULAR_UNITS").toString();
    qDebug()<<trajAngularUnits;

    QString trajNoForceHoming = qsettings->value("NO_FORCE_HOMING").toString();
    qDebug()<<trajNoForceHoming;

    QString trajMaxVel = qsettings->value("MAX_VELOCITY").toString();
    qDebug()<<trajMaxVel;

    QString trajMaxAcc = qsettings->value("MAX_ACCELERATION").toString();
    qDebug()<<trajMaxAcc;

    QString trajLinearUnits = qsettings->value("LINEAR_UNITS").toString();
    qDebug()<<trajLinearUnits;

    QString trajHome = qsettings->value("HOME").toString();
    qDebug()<<trajHome;

    QString trajDefVel = qsettings->value("DEFAULT_VELOCITY").toString();
    qDebug()<<trajDefVel;

    QString trajDefAcc = qsettings->value("DEFAULT_ACCELERATION").toString();
    qDebug()<<trajDefAcc;

    QString trajCycleTime = qsettings->value("CYCLE_TIME").toString();
    qDebug()<<trajCycleTime;

    QString trajCoordinates = qsettings->value("COORDINATES").toString();
    qDebug()<<trajCoordinates;

    QString trajAxis = qsettings->value("AXIS").toString();
    qDebug()<<trajAxis;

    QString trajArcBlendRampFreq  = qsettings->value("ARC_BLEND_RAMP_FREQ").toString();
    qDebug()<<trajArcBlendRampFreq;

    QString trajArcBlendOptiDepth = qsettings->value("ARC_BLEND_OPTIMIZATION_DEPTH").toString();
    qDebug()<<trajArcBlendOptiDepth;

    QString trajArcBlendGapCycles = qsettings->value("ARC_BLEND_GAP_CYCLES").toString();
    qDebug()<<trajArcBlendGapCycles;

    QString trajArcBlendFallbackEnable = qsettings->value("ARC_BLEND_FALLBACK_ENABLE").toString();
    qDebug()<<trajArcBlendFallbackEnable;

    QString trajArcBlendEnable = qsettings->value("ARC_BLEND_ENABLE").toString();
    qDebug()<<trajArcBlendEnable;


    ui->trajNoForceHomingEdit->setText(trajNoForceHoming);
    ui->trajMaxVelEdit_3->setText(trajMaxVel);
    ui->trajMaxAccEdit_3->setText(trajMaxAcc);
    ui->trajLinearUnitsEdit->setText(trajLinearUnits);
    ui->trajHomeEdit->setText(trajHome);
    ui->trajDefVelEdit_3->setText(trajDefVel);
    ui->trajDefAccEdit_3->setText(trajDefAcc);
    ui->trajCycleTimeEdit->setText(trajCycleTime);
    ui->trajCoordinatesEdit->setText(trajCoordinates);
    ui->trajAxisEdit->setText(trajAxis);
    ui->trajArcBlendRampFreqEdit->setText(trajArcBlendRampFreq);
    ui->trajArcBlendOptiDepthEdit->setText(trajArcBlendOptiDepth);
    ui->trajArcBlendGapCyclesEdit->setText(trajArcBlendGapCycles);
    ui->trajArcBlendFallbackEnableEdit->setText(trajArcBlendFallbackEnable);
    ui->trajArcBlendEnableEdit->setText(trajArcBlendEnable);
    ui->trajAngularUnitsEdit->setText(trajAngularUnits);
    ui->statusBar->clearMessage();
    ui->statusBar->setStyleSheet("background-color: ");

}

void MainWindow::on_xAxisBtn_clicked()
{
    ui->spindleBtn->setStyleSheet("color: ");
    ui->xAxisBtn->setStyleSheet("color: red");
    ui->yAxisBtn->setStyleSheet("color: ");
    ui->zAxisBtn->setStyleSheet("color: ");
    ui->trajAxisBtn->setStyleSheet("color: ");

    QSettings *qsettings = new QSettings("/home/automata/cnc_config/mydemo.inc", QSettings::IniFormat);
    qsettings->beginGroup("AXIS_0");
    ui->axisSelectionSTK->setCurrentIndex(1);
    currentIndex = 1;



    QString xBacklash = qsettings->value("BACKLASH").toString();
    qDebug()<<xBacklash;

    QString xCompFileType = qsettings->value("COMP_FILE_TYPE").toString();
    qDebug()<<xCompFileType;

    QString xFbScale = qsettings->value("FB_SCALE").toString();
    qDebug()<<xFbScale;

    QString xFerror = qsettings->value("FERROR").toString();
    qDebug()<<xFerror;

    QString xHome = qsettings->value("HOME").toString();
    qDebug()<<xHome;

    QString xHomeLatchVel = qsettings->value("HOME_LATCH_VEL").toString();
    qDebug()<<xHomeLatchVel;

    QString xHomeSearchVel = qsettings->value("HOME_SEARCH_VEL").toString();
    qDebug()<<xHomeSearchVel;

    QString xHomeSequence = qsettings->value("HOME_SEQUENCE").toString();
    qDebug()<<xHomeSequence;

    QString xHomeUseDrive = qsettings->value("HOME_USE_DRIVE").toString();
    qDebug()<<xHomeUseDrive;

    QString xHomeUseIndex = qsettings->value("HOME_USE_INDEX").toString();
    qDebug()<<xHomeUseIndex;

    QString xHomeVolatile = qsettings->value("VOLATILE_HOME").toString();
    qDebug()<<xHomeVolatile;

    QString xMaxAcc = qsettings->value("MAX_ACCELERATION").toString();
    qDebug()<<xMaxAcc;

    QString xMaxVel = qsettings->value("MAX_VELOCITY").toString();
    qDebug()<<xMaxVel;

    QString xMaxLimit = qsettings->value("MAX_LIMIT").toString();
    qDebug()<<xMaxLimit;

    QString xMinFerror = qsettings->value("MIN_FERROR").toString();
    qDebug()<<xMinFerror;

    QString xMinLimit = qsettings->value("MIN_LIMIT").toString();
    qDebug()<<xMinLimit;

    QString xScale = qsettings->value("SCALE").toString();
    qDebug()<<xScale;

    QString xType = qsettings->value("TYPE").toString();
    qDebug()<<xType;

    ui->xBacklashEdit_3->setText(xBacklash);
    ui->xCompFileTypeEdit->setText(xCompFileType);
    ui->xFbScaleEdit->setText(xFbScale);
    ui->xFerrorEdit_3->setText(xFerror);
    ui->xHomeEdit->setText(xHome);
    ui->xHomeLatchVelEdit->setText(xHomeLatchVel);
    ui->xHomeSearchVelEdit->setText(xHomeSearchVel);
    ui->xHomeSequenceEdit->setText(xHomeSequence);
    ui->xHomeUseDriveEdit->setText(xHomeUseDrive);
    ui->xHomeUseIndexEdit->setText(xHomeUseIndex);
    ui->xHomeVolatileEdit->setText(xHomeVolatile);
    ui->xMaxAccEdit_3->setText(xMaxAcc);
    ui->xMaxLimitEdit_3->setText(xMaxLimit);
    ui->xMaxVelEdit_3->setText(xMaxVel);
    ui->xMinFerrorEdit_3->setText(xMinFerror);
    ui->xMinLimitEdit_3->setText(xMinLimit);
    ui->xScaleEdit_3->setText(xScale);
    ui->xTypeEdit->setText(xType);
    ui->statusBar->clearMessage();
    ui->statusBar->setStyleSheet("background-color: ");



}

void MainWindow::on_yAxisBtn_clicked()
{
    ui->spindleBtn->setStyleSheet("color: ");
    ui->xAxisBtn->setStyleSheet("color: ");
    ui->yAxisBtn->setStyleSheet("color: red");
    ui->zAxisBtn->setStyleSheet("color: ");
    ui->trajAxisBtn->setStyleSheet("color: ");

    QSettings *qsettings = new QSettings("/home/automata/cnc_config/mydemo.inc", QSettings::IniFormat);
    qsettings->beginGroup("AXIS_1");
    ui->axisSelectionSTK->setCurrentIndex(2);
    currentIndex = 2;

    QString yBacklash = qsettings->value("BACKLASH").toString();
    qDebug()<<yBacklash;

    QString yCompFileType = qsettings->value("COMP_FILE_TYPE").toString();
    qDebug()<<yCompFileType;

    QString yFbScale = qsettings->value("FB_SCALE").toString();
    qDebug()<<yFbScale;

    QString yFerror = qsettings->value("FERROR").toString();
    qDebug()<<yFerror;

    QString yHome = qsettings->value("HOME").toString();
    qDebug()<<yHome;

    QString yHomeLatchVel = qsettings->value("HOME_LATCH_VEL").toString();
    qDebug()<<yHomeLatchVel;

    QString yHomeSearchVel = qsettings->value("HOME_SEARCH_VEL").toString();
    qDebug()<<yHomeSearchVel;

    QString yHomeSequence = qsettings->value("HOME_SEQUENCE").toString();
    qDebug()<<yHomeSequence;

    QString yHomeUseDrive = qsettings->value("HOME_USE_DRIVE").toString();
    qDebug()<<yHomeUseDrive;

    QString yHomeUseIndex = qsettings->value("HOME_USE_INDEX").toString();
    qDebug()<<yHomeUseIndex;

    QString yHomeVolatile = qsettings->value("VOLATILE_HOME").toString();
    qDebug()<<yHomeVolatile;

    QString yMaxAcc = qsettings->value("MAX_ACCELERATION").toString();
    qDebug()<<yMaxAcc;

    QString yMaxVel = qsettings->value("MAX_VELOCITY").toString();
    qDebug()<<yMaxVel;

    QString yMaxLimit = qsettings->value("MAX_LIMIT").toString();
    qDebug()<<yMaxLimit;

    QString yMinFerror = qsettings->value("MIN_FERROR").toString();
    qDebug()<<yMinFerror;

    QString yMinLimit = qsettings->value("MIN_LIMIT").toString();
    qDebug()<<yMinLimit;

    QString yScale = qsettings->value("SCALE").toString();
    qDebug()<<yScale;

    QString yType = qsettings->value("TYPE").toString();
    qDebug()<<yType;

    QString yHomeOffset = qsettings->value("HOME_OFFSET").toString();
    qDebug()<<yHomeOffset;

    ui->yBacklashEdit_3->setText(yBacklash);
    ui->yCompFileTypeEdit->setText(yCompFileType);
    ui->yFbScaleEdit->setText(yFbScale);
    ui->yFerrorEdit_3->setText(yFerror);
    ui->yHomeEdit->setText(yHome);
    ui->yHomeLatchVelEdit->setText(yHomeLatchVel);
    ui->yHomeSearchVelEdit->setText(yHomeSearchVel);
    ui->yHomeSeqEdit->setText(yHomeSequence);
    ui->yHomeUseDriveEdit->setText(yHomeUseDrive);
    ui->yHomeUseIndexEdit->setText(yHomeUseIndex);
    ui->yHomeVolatileEdit->setText(yHomeVolatile);
    ui->yMaxAccEdit_3->setText(yMaxAcc);
    ui->yMaxLimitEdit_3->setText(yMaxLimit);
    ui->yMaxVelEdit_3->setText(yMaxVel);
    ui->yMinFerrorEdit_3->setText(yMinFerror);
    ui->yMinLimitEdit_3->setText(yMinLimit);
    ui->yScaleEdit_3->setText(yScale);
    ui->yTypeEdit->setText(yType);
    ui->yHomeOffsetEdit->setText(yHomeOffset);
    ui->statusBar->clearMessage();
    ui->statusBar->setStyleSheet("background-color: ");
}

void MainWindow::on_zAxisBtn_clicked()
{
    ui->spindleBtn->setStyleSheet("color: ");
    ui->xAxisBtn->setStyleSheet("color: ");
    ui->yAxisBtn->setStyleSheet("color: ");
    ui->zAxisBtn->setStyleSheet("color: red");
    ui->trajAxisBtn->setStyleSheet("color: ");

    QSettings *qsettings = new QSettings("/home/automata/cnc_config/mydemo.inc", QSettings::IniFormat);
    qsettings->beginGroup("AXIS_2");
    ui->axisSelectionSTK->setCurrentIndex(3);
    currentIndex = 3;

    QString zBacklash = qsettings->value("BACKLASH").toString();
    qDebug()<<zBacklash;

    QString zCompFileType = qsettings->value("COMP_FILE_TYPE").toString();
    qDebug()<<zCompFileType;

    QString zFbScale = qsettings->value("FB_SCALE").toString();
    qDebug()<<zFbScale;

    QString zFerror = qsettings->value("FERROR").toString();
    qDebug()<<zFerror;

    QString zHome = qsettings->value("HOME").toString();
    qDebug()<<zHome;

    QString zHomeLatchVel = qsettings->value("HOME_LATCH_VEL").toString();
    qDebug()<<zHomeLatchVel;

    QString zHomeSearchVel = qsettings->value("HOME_SEARCH_VEL").toString();
    qDebug()<<zHomeSearchVel;

    QString zHomeSequence = qsettings->value("HOME_SEQUENCE").toString();
    qDebug()<<zHomeSequence;

    QString zHomeUseDrive = qsettings->value("HOME_USE_DRIVE").toString();
    qDebug()<<zHomeUseDrive;

    QString zHomeUseIndex = qsettings->value("HOME_USE_INDEX").toString();
    qDebug()<<zHomeUseIndex;

    QString zHomeVolatile = qsettings->value("VOLATILE_HOME").toString();
    qDebug()<<zHomeVolatile;

    QString zMaxAcc = qsettings->value("MAX_ACCELERATION").toString();
    qDebug()<<zMaxAcc;

    QString zMaxVel = qsettings->value("MAX_VELOCITY").toString();
    qDebug()<<zMaxVel;

    QString zMaxLimit = qsettings->value("MAX_LIMIT").toString();
    qDebug()<<zMaxLimit;

    QString zMinFerror = qsettings->value("MIN_FERROR").toString();
    qDebug()<<zMinFerror;

    QString zMinLimit = qsettings->value("MIN_LIMIT").toString();
    qDebug()<<zMinLimit;

    QString zScale = qsettings->value("SCALE").toString();
    qDebug()<<zScale;

    QString zType = qsettings->value("TYPE").toString();
    qDebug()<<zType;

    QString zHomeOffset = qsettings->value("HOME_OFFSET").toString();
    qDebug()<<zHomeOffset;

    ui->zBacklashEdit_3->setText(zBacklash);
    ui->zCompFileTypeEdit->setText(zCompFileType);
    ui->zFbScaleEdit->setText(zFbScale);
    ui->zFerrorEdit_3->setText(zFerror);
    ui->zHomeEdit->setText(zHome);
    ui->zHomeLatchVelEdit->setText(zHomeLatchVel);
    ui->zHomeSearchVelEdit->setText(zHomeSearchVel);
    ui->zHomeSeqEdit->setText(zHomeSequence);
    ui->zHomeUseDriveEdit->setText(zHomeUseDrive);
    ui->zHomeUseIndexEdit->setText(zHomeUseIndex);
    ui->zVolatileHomeEdit->setText(zHomeVolatile);
    ui->zMaxAccEdit_3->setText(zMaxAcc);
    ui->zMaxLimitEdit_3->setText(zMaxLimit);
    ui->zMaxVelEdit_3->setText(zMaxVel);
    ui->zMinFerrorEdit_3->setText(zMinFerror);
    ui->zMinLimitEdit_3->setText(zMinLimit);
    ui->zScaleEdit_3->setText(zScale);
    ui->zTypeEdit->setText(zType);
    ui->zHomeOffsetEdit->setText(zHomeOffset);
    ui->statusBar->clearMessage();
    ui->statusBar->setStyleSheet("background-color: ");
//    palette.setColor(QPalette::WindowText,Color_Status);

}

void MainWindow::on_importZBtn_clicked()
{
    QString source = "/home/automata/cnc_config/";
    QString source_path = "/home/automata/cnc_config/mydemo.inc";
    QString temp_path = "/home/automata/Temp_location/mydemotemp.inc";
    QString final_path = "/home/automata/Final_location/mydemofinal.inc";
    QString final_path_copy = "/home/automata/Final_location/mydemocopy.inc";
    QString md5HexSource;
    QString md5HexTemp;
    QString md5HexFinal;

    fileReplaceDialog *dialog = new fileReplaceDialog();

    dialog->exec();
    if(dialog->status_file_transfer == true)
    {

        if (file_presence(source_path) == 0)                        // file presence source if yes md5 generation of source
        {
            qDebug()<<"file present source";
            md5HexSource = md5_generation(source_path);
            qDebug()<<"md5 of Source"<<md5HexSource;
            if(file_presence(temp_path) == 0)                       // //file presence temp if temp file present yes then remove and copy
            {
                bool remove_temp = QFile::remove(temp_path);
                qDebug()<<"Remove Temp"<<remove_temp;
            }

            bool copy_temp = QFile::copy(source_path,temp_path);
            qDebug()<<"Temp File Copied"<<copy_temp;

            if(file_presence(temp_path) == 0)                       //after comparing checking again the presence of temp file
            {
                qDebug()<<"file present temp";
                md5HexTemp = md5_generation(temp_path);                 // if yes generation of md5 file for temp
                qDebug()<<"md5 of Temp"<<md5HexTemp;
                if(compare_md5(md5HexSource,md5HexTemp) == 0)         // compare the md5 sum of the source and temp// if yes then checking of presence of final file
                {


                    if(file_presence(final_path) == 0)              // if present of final file remove and replace
                    {
                        if(file_presence(final_path_copy) == 0)
                        {
                            bool remove = QFile::remove(final_path_copy);

                            qDebug()<<"replace final file remove for replace"<<remove;
                        }
                        QFile::copy(final_path,final_path_copy);
                        bool remove_final = QFile::remove(final_path);
                        qDebug()<<"Remove Final"<<remove_final;
                    }
                    bool copy_final = QFile::copy(temp_path,final_path);
                    qDebug()<<"Final File Copied"<<copy_final;

                    if(file_presence(final_path) == 0)              //after copying checking of presence of final file// if yes then generation og md5 sum of final file
                    {
                        qDebug()<<"Final File Present";
                        md5HexFinal = md5_generation(final_path);
                        qDebug()<<"md5 of Final"<<md5HexFinal;

                        if(compare_md5(md5HexTemp,md5HexFinal) == 0)  // comparing of the md5 sumo fthe temp and the final file// if compare true then display final transfer complete
                        {
                            qDebug()<<"File transfer is complete";

                            ui->statusBar->showMessage("FILE TRANSFER COMPLETE");
                            ui->statusBar->setStyleSheet("background-color: lightgreen");

                        }

                        else
                        {
                            qDebug()<<"Temp and final md5 comaprison fail";//final file comarison fail
                            ui->statusBar->showMessage("FILE TRANSFER ERROR PLEASE TRY AGAIN");
                            ui->statusBar->setStyleSheet("background-color: red");
                        }
                    }
                    else                                            //final file not found after copying
                    {
                        qDebug()<<"Final file not found";
                        ui->statusBar->showMessage("FILE TRANSFER ERROR PLEASE TRY AGAIN");
                        ui->statusBar->setStyleSheet("background-color: red");
                    }
                }


                else                                                //comparison of the source and temp file fail
                {
                    qDebug()<<"Source and Temp md5 comparison Fail";
                    ui->statusBar->showMessage("FILE TRANSFER ERROR PLEASE TRY AGAIN");
                    ui->statusBar->setStyleSheet("background-color: red");
                }
            }

            else                                                    //temp file not present after copying
            {
                qDebug()<<"Temp file not found";
                ui->statusBar->showMessage("FILE TRANSFER ERROR PLEASE TRY AGAIN");
                ui->statusBar->setStyleSheet("background-color: red");

            }


        }
        else                                                        //source file not found
        {
            qDebug()<<"source file not found";
            ui->statusBar->setStyleSheet("background-color: red");
            ui->statusBar->showMessage("SOURCE FILE NOT FOUND");

        }

    }

//    if(fileName == false)
//    {
//        ui->statusBar->setStyleSheet("background-color: red");
//        ui->statusBar->showMessage("SOURCE FILE NOT FOUND");
//    }



}



QString MainWindow::md5_generation(QString md5path)
{

    QFile filemd5(md5path);
    QByteArray fileData = filemd5.readAll();
    QByteArray hashdata = QCryptographicHash::hash(fileData, QCryptographicHash::Md5);
    QString hashdatahex = hashdata.toHex();
    qDebug()<<"md5 hex"<<hashdatahex;
    return hashdatahex;
}

int MainWindow::file_presence(QString path)
{
//    QFile fileSrc(path);
    bool pathexists = QFile::exists(path);
    qDebug()<<"File Exists"<<pathexists;

    if(pathexists == true)
    {
        return 0;
    }
    else
    {
        qDebug()<<"Error";
        return 1;
    }

}

int MainWindow::compare_md5(QString compare_md5path,QString compare_md5path2)
{
    qDebug()<<"1st path hex file"<<compare_md5path;
    qDebug()<<"2nd path hex file"<<compare_md5path2;
    int x = QString::compare(compare_md5path, compare_md5path2, Qt::CaseInsensitive);
    qDebug()<<"x"<<x;
    if(x == 0)
    {
        return 0;
        qDebug()<<"Compring successfull";
    }
    else
    {
        return 1;
        qDebug()<<"Comparing fail";
    }


}



void MainWindow::on_factoryResetBtn_clicked()
{
    passwordDialog *pass = new passwordDialog();
    pass->exec();
    QString final_path = "/home/automata/Final_location/mydemofinal.inc";
    QString factory_path = "/home/automata/Factory_location/mydemofactory.inc";

    if(pass->status_password == 0)
    {
        qDebug()<<"Entered the loop"<<file_presence(factory_path);
        if(file_presence(factory_path) == 0)
        {
            qDebug()<<"Entered loop 2"<<file_presence(factory_path);
           if(file_presence(final_path) == 0)
            {
                bool remove_factory = QFile::remove(final_path);
                qDebug()<<"Final file present and removed for factory"<<remove_factory;
            }
            bool factory_copy = QFile::copy(factory_path,final_path);
            qDebug()<<"factory file copied "<<factory_copy;

        }
        ui->statusBar->showMessage("FACTORY FILE TRANSFER COMPLETE");
        ui->statusBar->setStyleSheet("background-color: lightgreen");

    }
    else if (pass->status_password == 1)
    {
        ui->statusBar->setStyleSheet("background-color: red");
        ui->statusBar->showMessage("WRONG PASSWORD ENTERED PLEASE TRY AGAIN");
    }

    else
    {
        ui->statusBar->setStyleSheet("background-color: red");
        ui->statusBar->showMessage("FACTORY RESET ABORTED");

    }
}
