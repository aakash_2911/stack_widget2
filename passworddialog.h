#ifndef PASSWORDDIALOG_H
#define PASSWORDDIALOG_H

#include <QDialog>

namespace Ui {
class passwordDialog;
}

class passwordDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit passwordDialog(QWidget *parent = 0);
    ~passwordDialog();
    int status_password;
    
private slots:
    void on_okBtn_clicked();

    void on_cancelBtn_clicked();

private:
    Ui::passwordDialog *ui;
};

#endif // PASSWORDDIALOG_H
