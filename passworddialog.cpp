#include "passworddialog.h"
#include "ui_passworddialog.h"
#include <QDebug>

passwordDialog::passwordDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::passwordDialog)
{
    ui->setupUi(this);
//    ui->passwordEdit->isReadOnly(false);
}

passwordDialog::~passwordDialog()
{
    delete ui;
}

void passwordDialog::on_okBtn_clicked()
{
    QString original_password = "automata";
    QString password = (ui->passwordEdit->text());
    if(original_password == password)
    {
        status_password = 0;
        qDebug()<<"Password Match"<<status_password;
    }
    else
    {
        status_password = 1;
    }
    this->close();
}

void passwordDialog::on_cancelBtn_clicked()
{
    status_password = 2;
    this->close();
}
